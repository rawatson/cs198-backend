'use strict';
module.exports = function(sequelize, DataTypes) {
  var PaperlessComment = sequelize.define('paperless_comments', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    start_line: DataTypes.INTEGER,
    end_line: DataTypes.INTEGER,
    comment_type: DataTypes.ENUM('Functionality', 'Style', 'Other'),
    comment: DataTypes.STRING,
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        //PaperlessComment.belongsTo(models.courses, {foreignKey: 'course_id'});
      }
    }
  });
 
  return PaperlessComment;
};
