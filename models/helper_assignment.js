'use strict';
module.exports = function(sequelize, DataTypes) {
  var HelperAssignment = sequelize.define('helper_assignments', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    helper_checkin_id: DataTypes.INTEGER,
    help_request_id: DataTypes.INTEGER,
    claim_time: DataTypes.DATE,
    close_time: DataTypes.DATE,
    reassignment_id: DataTypes.INTEGER
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        HelperAssignment.belongsTo(models.helper_checkins, {foreignKey: 'helper_checkin_id'});
        HelperAssignment.belongsTo(models.help_requests, {foreignKey: 'help_request_id'});
        HelperAssignment.belongsTo(models.helper_assignments, {foreignKey: 'reassignment_id'});
      }
    }
  });
 
  return HelperAssignment;
};
