'use strict';
module.exports = function(sequelize, DataTypes) {
  var Submission = sequelize.define('submissions', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    student_relation_id: DataTypes.INTEGER,
    assignment_id: DataTypes.INTEGER,
    submission_time: DataTypes.DATE,
    submission_path: DataTypes.STRING
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Submission.belongsTo(models.student_relations, {foreignKey: 'student_relation_id'});
      }
    }
  });
 
  return Submission;
};
