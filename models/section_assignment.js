'use strict';
module.exports = function(sequelize, DataTypes) {
  var SectionAssignments = sequelize.define('section_assignments', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    section_id: DataTypes.INTEGER,
    student_id: DataTypes.INTEGER,
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        SectionAssignments.belongsTo(models.people, {foreignKey: 'student_id'});
        SectionAssignments.belongsTo(models.sections, {foreignKey: 'section_id'});
      }
    }
  });
 
  return SectionAssignments;
};
