'use strict';
module.exports = function(sequelize, DataTypes) {
  var HelperShift = sequelize.define('helper_shifts', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    start_time: DataTypes.DATE,
    duration_minutes: DataTypes.INTEGER,
    regular_shift: DataTypes.BOOLEAN,
    person_id: DataTypes.INTEGER
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        HelperShift.belongsTo(models.people, {foreignKey: 'person_id'});
      }
    }
  });
 
  return HelperShift;
};
