'use strict';
module.exports = function(sequelize, DataTypes) {
  var GlobalState = sequelize.define('global_state', {
    lair_signups_enabled: DataTypes.BOOLEAN,
    current_quarter_id: DataTypes.INTEGER
  }, {
    timestamps: false,
    underscored: true,
    tableName: 'global_state',
    classMethods: {
      associate: function(models) {

      }
    }
  });
  GlobalState.removeAttribute('id');
 
  return GlobalState;
};
