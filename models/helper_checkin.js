'use strict';
module.exports = function(sequelize, DataTypes) {
  var HelperCheckin = sequelize.define('helper_checkins', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    person_id: DataTypes.INTEGER,
    check_in_time: DataTypes.DATE,
    check_out_time: DataTypes.DATE
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        HelperCheckin.belongsTo(models.people, {foreignKey: 'person_id'});
        HelperCheckin.hasMany(models.helper_assignments, {foreignKey: 'helper_checkin_id'});
      }
    }
  });
 
  return HelperCheckin;
};
