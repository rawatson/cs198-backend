'use strict';
module.exports = function(sequelize, DataTypes) {
  var Sections = sequelize.define('sections', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    course_id: DataTypes.INTEGER,
    section_leader_id: DataTypes.INTEGER,
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Sections.belongsTo(models.people, {
          foreignKey: 'section_leader_id',
          as: 'section_leader'
        });

        Sections.belongsTo(models.courses, {foreignKey: 'course_id'});
      }
    }
  });
 
  return Sections;
};
