'use strict';
module.exports = function(sequelize, DataTypes) {
  var Quarter = sequelize.define('quarters', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    year: DataTypes.INTEGER,
    quarter_name: DataTypes.ENUM('Autumn', 'Winter', 'Spring', 'Summer')
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {

      }
    }
  });
 
  return Quarter;
};
