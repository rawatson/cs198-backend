'use strict';
module.exports = function(sequelize, DataTypes) {
  var Course = sequelize.define('courses', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    department_code: DataTypes.STRING,
    course_number: DataTypes.STRING,
    quarter_id: DataTypes.INTEGER
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Course.belongsTo(models.quarters, {foreignKey: 'quarter_id'});
        Course.hasMany(models.staff_relations);
        Course.hasMany(models.student_relations);
        Course.hasMany(models.group_relations);
      }
    }
  });
 
  return Course;
};
