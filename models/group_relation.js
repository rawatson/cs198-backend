'use strict';
module.exports = function(sequelize, DataTypes) {
  var GroupRelations = sequelize.define('group_relations', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    person_id: DataTypes.INTEGER,
    course_id: DataTypes.INTEGER,
    group_name: DataTypes.STRING
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        GroupRelations.belongsTo(models.people, {foreignKey: 'person_id'});
        GroupRelations.belongsTo(models.courses, {foreignKey: 'course_id'});
      }
    }
  });
 
  return GroupRelations;
};
