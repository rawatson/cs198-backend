'use strict';
module.exports = function(sequelize, DataTypes) {
  var HelpRequest = sequelize.define('help_requests', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    student_relation_id: DataTypes.INTEGER,
    problem_description: DataTypes.STRING,
    location: DataTypes.STRING,
    request_time: DataTypes.DATE,
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        HelpRequest.belongsTo(models.student_relations, {foreignKey: 'student_relation_id'});
        HelpRequest.hasOne(models.helper_assignments, {foreignKey: 'help_request_id'});
      }
    }
  });
 
  return HelpRequest;
};
