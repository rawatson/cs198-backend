'use strict';
module.exports = function(sequelize, DataTypes) {
  var AssignmentGrade = sequelize.define('assignment_grades', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    assignment_id: DataTypes.INTEGER,
    student_relation_id: DataTypes.INTEGER,
    functionality_grade: DataTypes.STRING,
    style_grade: DataTypes.STRING,
    late_days_used: DataTypes.INTEGER
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        AssignmentGrade.belongsTo(models.student_relations, {foreignKey: 'student_relation_id'});
      }
    }
  });
 
  return AssignmentGrade;
};
