'use strict';
module.exports = function(sequelize, DataTypes) {
  var Assignment = sequelize.define('assignments', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    course_id: DataTypes.INTEGER,
    due_date: DataTypes.DATE,
    late_dates: DataTypes.JSON,
    assignment_name: DataTypes.STRING,
    assignment_number: DataTypes.FLOAT,
    required_files: DataTypes.JSON,
    hidden_file_extensions: DataTypes.JSON
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Assignment.belongsTo(models.courses, {foreignKey: 'course_id'});
        Assignment.hasMany(models.submissions, {foreignKey: 'assignment_id'});
        Assignment.hasMany(models.assignment_grades, {foreignKey: 'assignment_id'});
      }
    }
  });
 
  return Assignment;
};
