'use strict';
module.exports = function(sequelize, DataTypes) {
  var StudentRelations = sequelize.define('student_relations', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    person_id: DataTypes.INTEGER,
    course_id: DataTypes.INTEGER,
    listed_in_axess: DataTypes.BOOLEAN
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        StudentRelations.belongsTo(models.people, {foreignKey: 'person_id'});
        StudentRelations.belongsTo(models.courses, {foreignKey: 'course_id'});
      }
    }
  });
 
  return StudentRelations;
};
