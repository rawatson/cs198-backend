'use strict';
module.exports = function(sequelize, DataTypes) {
  var Person = sequelize.define('people', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    sunetid: DataTypes.STRING,
    suid: DataTypes.INTEGER,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    nick_name: DataTypes.STRING,
    email: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    gender: DataTypes.ENUM('Male', 'Female', 'Other', 'Unknown'),
    citizen_status: DataTypes.ENUM('US Citizen', 'Permanent Resident', 'International', 'Unknown'),
    staff_status: DataTypes.ENUM('Non-Staff', 'Active', 'Inactive / Alumni', 'Courtesy', 'Bad Standing'),
    hire_quarter_id: DataTypes.INTEGER
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Person.belongsTo(models.quarters, {foreignKey: 'hire_quarter_id', as: 'hire_quarter', onDelete: 'RESTRICT'});
      }
    }
  });
 
  return Person;
};
