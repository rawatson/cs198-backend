'use strict';
module.exports = function(sequelize, DataTypes) {
  var StaffRelations = sequelize.define('staff_relations', {
    id: {type: DataTypes.INTEGER, autoIncrement: true},
    person_id: DataTypes.INTEGER,
    course_id: DataTypes.INTEGER,
    position: DataTypes.ENUM('Course Helper', 'Grader', 'Section Leader',
                             'Head TA', 'Lecturer', 'Coordinator')
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        StaffRelations.belongsTo(models.people, {foreignKey: 'person_id'});
        StaffRelations.belongsTo(models.courses, {foreignKey: 'course_id'});
      }
    }
  });
 
  return StaffRelations;
};
