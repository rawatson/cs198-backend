var querystring = require('querystring');
var jwt = require('jsonwebtoken');
var db = require('../models/index');


var positions = [
  'Course Helper',
  'Grader',
  'Section Leader',
  'Head TA',
  'Lecturer',
  'Coordinator'
];

var auth = {};

function sendAuthenticationRequest(req, res) {
  var authURL = process.env.AUTHENTICATION_SCRIPT_URL;
  var opts = {};
  opts.setcookie_url = `http://${req.get('HOST')}/auth_tokens`;

  // If we're running in a debug setting, tell the authentication provider to
  // use a fixed, publicly known secret key.  This will destroy any security
  // that we might have, since anyone who know the secret key can create
  // arbitrary login tokens.  We need to do this to debug though, since you
  // don't want to require access to the production secret key in order to run
  // a debug instance.  In production, the secret key will not be sent on the
  // wire.  The only two parties who know the production secret key will be the
  // auth script and the production .env file.
  if (process.env.NODE_ENV != 'production')
    opts.secret_key = process.env.AUTH_SCRIPT_KEY;

  authURL += '?' + querystring.stringify(opts);
  res.json({
    authenticationRequiredError: 'You must be logged in to access this endpoint',
    authenticationURL: authURL
  });
}

function getJWT(req, res) {
  // Make sure we don't load the JWT more than once
  if (req.jwt)
    return req.jwt;

  var token = req.cookies.jwt
  if (!token) {
    console.log("JWT cookie not set, directing client to authenticate");
    sendAuthenticationRequest(req, res);
    return false;
  }
  console.log("JWT detected, validating now");

  var auth_key = process.env.AUTH_SCRIPT_KEY;
  try {
    token = jwt.verify(token, auth_key);
    req.jwt = token;
    return token;
  } catch (err) {
    console.log("JWT cookie not valid, requesting client re-authenticate");
    sendAuthenticationRequest(req, res);
    return false;
  }
}

function fastPathAuthenticate(jwt, position, course) {
  // TODO: It's possible to avoid a DB query on some calls by caching some of
  // the course_relation info in the token itself.  If we have time we should
  // add this functionality, since it has the potential to save a blocking DB
  // round trip on every single authenticated request
  console.log("Fast path auth not implemented!");
  return false;
}

function* slowPathAuthenticate(req, res, position, course) {
  console.log("Starting slow path authentication");
  var staff_relations = yield db.staff_relations.findAll({
    include: {model: db.people, where: {sunetid: req.jwt.sunetid}, attributes: []} 
  });
 
  // TODO: Determine what kinds of authentication we want to have
  return true;
}

auth.sendAccessDenied = function(req, res) {
  // TODO: Send a specially coded error which will redirect them to an access
  // denied page maybe?
  res.json({error: "access denied"});
}


// TODO: I haven't finished writing this function since I'm not sure who's
// going to be using it.  I'll revisit this once we have a better understanding
// of auth client needs.
auth.requireStaffRelation = function*(req, res, position, course) {
  if (!getJWT(req, res))
    return false;

  // Try fast-path authentication (using JWT)
  if (fastPathAuthenticate(jwt, position, course))
    return true;

  // If that didn't conclusively answer, try slow path
  if (yield* slowPathAuthenticate(req, res, position, course))
    return true;

  // If slow path auth says no, then it's definitely a no.  Return access denied
  auth.sendAccessDenied(req, res);
  return false;
}

auth.requireValidLogin = function*(req, res) {
  // Get the JWT
  if (!getJWT(req, res))
    return false;

  return true;
}

auth.requireCurrentStaff = function*(req, res) {
  if (!getJWT(req, res))
    return false;

  // TODO: Finish this
}

module.exports = auth;
