"use strict";
var db = require('../../models/index');
var moment = require('moment');
var express = require('express');
var app = express.application;
var legacy_db = app.legacy_db;


var people = {};
people.show = function*(req, res) {
  var name = req.params.person;
  var person = yield db.people.findOne({where: {sunetid: name}});
  if (person !== null) {
    res.json({'data': person});
  } else {
    throw `Cannot find a person with SUNetID ${name}`;
  }
}

people.update = function*(req, res) {
  var name = req.params.id;
  var person = yield db.people.update(req.body, {where: {sunetid: name}, returning: true});
  res.json({data: person[0]});
}

// Handle retrieving an individual's help request history
people.help_requests = {};
people.help_requests.index = function*(req, res) {
  var sunet = req.params.person;

  // If necessary, filter by when requests were created
  var request_time_condition = {};
  if (req.query.desired_history) {
    var threshold = moment().subtract(req.query.desired_history, 'minutes');
    request_time_condition = {request_time: {$gte: threshold.format()}};
  }

  var help_requests = yield db.help_requests.findAll({
    include: [
      {
        model: db.student_relations,
        attributes: [],
        include: {
          model: db.people,
          attributes: [],
          where: {sunetid: sunet}
        }
      },
      db.helper_assignments
    ],
    where: request_time_condition
  });
  res.send({'data': help_requests});
}

//
//
// Allow clients to check which courses a student has taken
//
//
people.student_relations = {};
people.student_relations.index = function*(req, res) {
  var person_id = req.params.person;

  var courses = yield db.student_relations.findAll({
    include: [
      {
        model: db.courses,
        include: db.quarters,
      }
    ],
    where: {person_id: person_id}
  });

  res.json({'data': courses});
}

//
//
// Allow clients to check which courses a staff is related to
//
//
people.staff_relations = {};
people.staff_relations.index = function*(req, res) {
  var person_id = req.params.person;
  var courses = yield db.staff_relations.findAll({
    include: [
      {
        model: db.courses,
        include: db.quarters,
      }
    ],
    where: {person_id: person_id}
  });

  res.json({'data': courses});
}




//
//
// Code for getting a given staff member's sections
//
//
people.sections = {};
function getLegacyCourseID(course_number) {
   var legacy_course_id_mapping = {
    '106a': 1,
    '106b': 2,
    '106x': 3,
    '198': 8
  };
  return  legacy_course_id_mapping[course_number];
}

// Returns [year, quarter] for a modern quarter id
function getLegacyQuarterInfo(qid) {
  var base_year = 1900 + Math.floor(qid / 10);
  var year = base_year - (qid % 10 == 2 ? 1 : 0);
  var quarter_mapping = {
    2: 4,
    4: 1,
    6: 2,
    8: 3
  }
  var quarter = quarter_mapping[qid % 10];
  return [year, quarter];
}

function checkIfSL(person, course) {
  return db.sequelize.query(`
    SELECT
      COUNT(*)
    FROM
      staff_relations
      INNER JOIN people ON staff_relations.person_id = people.id
    WHERE
      people.sunetid = ?
      AND staff_relations.course_id = ?
      AND staff_relations.position IN ('Section Leader', 'Grader', 'Course Helper')
  `, {replacements: [person, course]})
  .then(function(result) {
    return result[0][0].count != 0;
  });
}

// Get the sections for a given staff member for a given course
// Example: GET /people/rawatson/sections?course=959
//
// If you're a Coord, Head TA, or lecturer in that quarter you'll get a listing
// of all sections in the class Otherwise, you'll get your own section.
people.sections.index = function*(req, res) {
  var person = req.params.person;

  var course = req.query.course_id;
  if (!course) {
    res.json({error: 'You must specifiy a course to get sections for!'});
    return;
  }

  var isSL = yield checkIfSL(person, course);

  var course_data = yield db.sequelize.query(`
    SELECT
      course_number AS course_number,
      quarter_id AS quarter
    FROM
      courses
    WHERE
      ID = ?
  `, {replacements: [course]});

  var legacy_course_id = getLegacyCourseID(course_data[0][0].course_number);
  var quarter_info = getLegacyQuarterInfo(course_data[0][0].quarter);
  var replacements = [legacy_course_id, quarter_info[0], quarter_info[1]];

  var query = `
    SELECT
      SectionStudent.SUNetID AS Student,
      SectionLeader.SUNetID AS SL
    FROM
      SectionAssignments
      INNER JOIN People AS SectionStudent ON SectionStudent.ID = SectionAssignments.Person
      INNER JOIN Sections ON SectionAssignments.Section = Sections.ID
      INNER JOIN People AS SectionLeader ON Sections.SectionLeader = SectionLeader.ID
      INNER JOIN Quarters ON Sections.Quarter = Quarters.ID
    WHERE
      Sections.Class = ?
      AND Quarters.Year = ?
      AND Quarters.Quarter = ?
  `;

  if (isSL) {
    query += `
      AND Sections.SectionLeader = (SELECT ID FROM People WHERE SUNetID = ?)
    `;
    replacements.push(person);
  }

  var sections = yield legacy_db.query(query, replacements);
  res.json({data: sections[0]});
}

module.exports = people;
