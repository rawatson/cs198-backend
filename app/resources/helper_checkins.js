"use strict";
var db = require('../../models/index');
var _ = require('lodash');

var helper_checkins = {};
helper_checkins.index = function*(req, res) {
  var helpers = yield db.helper_checkins.findAll({
    where: {check_out_time: null}, 
    include: [db.people, {model: db.helper_assignments, where: {close_time: null}, required: false}]
  });

  helpers = helpers.map(function(helper) {
    helper = helper.toJSON();
    if (helper.helper_assignments.length !== 0) {
      helper.currently_helping = helper.helper_assignments[0];
    }
    helper.helper_assignments = undefined;
    return helper;
  });
  res.json({'data': helpers});
}

helper_checkins.create = function*(req, res) {
  var sunetid = req.body.person;
  // Check that the person in question is a real staff member
  var person = yield db.people.findOne({where: {sunetid: sunetid}});
  if (person === null) {
    throw `Cannot find a person named ${sunetid}`;
  } else if (person.staff_status === `Non-Staff`) {
    throw `${sunetid} is not a staff member!`;
  }

  // Check that the person in question isn't already logged in
  var currentHelpers = yield db.helper_checkins.findAll({where: {check_out_time: null}});
  if (currentHelpers.some(function(h) {return h.person_id === person.id;})) {
    throw `${sunetid} is already checked in`;
  }

  // Log them in and return a response
  var result = yield db.helper_checkins.create({person_id: person.id}, {returning: true});

  var response = result.toJSON();
  response.person = person.toJSON();
  res.json({data: response});
}

helper_checkins.destroy = function*(req, res) {
  yield db.sequelize.query(`UPDATE helper_checkins SET check_out_time = now() WHERE id = ?`, 
      {replacements: [req.params.helper_checkin]});
   res.json({});
}
module.exports = helper_checkins;
