"use strict";
var db = require('../../models/index');

var help_requests = {};

/* Returns all unassigned & unresolved help requests */
help_requests.index = function*(req, res) {
  // Ignore all help requests which have ever been assigned a helper
  let ignoreSQL = `(SELECT help_request_id FROM helper_assignments)`;
  let idsToIgnore = db.sequelize.literal(ignoreSQL);
  
  let help_requests = yield db.help_requests.findAll({
    include: {
      model: db.student_relations,
      include: [db.people, db.courses]
    },
    where: {id: {$notIn: idsToIgnore}}
  });

  res.json({data: help_requests});
}

/* Create a new help request.
 * Note: Help request resolves are handled in helper_assignments.js */
help_requests.create = function*(req, res) {
  var person_id = req.body.person_id;
  var course_id = req.body.course_id;
  var location = req.body.location;
  var problem_description = req.body.problem_description;

  if (location === '')
    throw 'Missing location';
  if (problem_description === '')
    throw 'Missing problem description';

  var student_relation_id = yield db.student_relations.findOne(
                               {attributes: ['id'],
                                where: {person_id: person_id, course_id: course_id}});
  student_relation_id = student_relation_id.dataValues.id;
  
  var result = yield db.help_requests.create({student_relation_id: student_relation_id,
                                              problem_description: problem_description,
                                              location: location},
                                             {returning: true});
  res.send({data: result});
}

module.exports = help_requests;
