"use strict";
var db = require('../../models/index');

var helper_assignments = {};

/* Returns all assigned help requests.
 * Use query strings to return either resolved or unresolved.
 *
 * For resolved requests, only returns requests that have been closed after "since"
 */
helper_assignments.index = function*(req, res) {
  var resolved = (req.query.resolved === 'true');
  var since = new Date(req.query.since);

  var resolved_condition = resolved ? {close_time : {$and : {$ne :null, $gte :since}}} :
                                      {close_time : null};

  var help_requests = yield db.help_requests.findAll({
    include: [
      {model: db.student_relations, include: [db.people, db.courses]},
      {
        model: db.helper_assignments,
        include: {model: db.helper_checkins, include: db.people},
        where: resolved_condition
      }
    ],
  });

  res.json({data: help_requests});
}

/* Create a new helper assignment.
 * TODO (hgkshin): Error check to make sure no duplicates
 *                 Rename reassignment_id to original_helper_checkin_id
 */
helper_assignments.create = function*(req, res) {
  var helper_checkin_id = req.body.helper_checkin_id;
  var help_request_id = req.body.help_request_id;

  var currentAssignments = yield db.helper_assignments.findAll({where: {close_time: null}});
  if (currentAssignments.some(function(a) {return a.help_request_id === help_request_id;})) {
    res.json({error: 'Someone has already claimed this help request'});
    return;
  }

  var result = yield db.helper_assignments.create(
    {helper_checkin_id: helper_checkin_id, help_request_id: help_request_id},
    {returning: true}
  );

  res.send({data: result});
}

/* Update an old helper assignment (re-assign helper or re-open (mark unresolved)) */
helper_assignments.update = function*(req, res) {
  var helper_assignment_id = req.params.helper_assignment;
  var new_helper_checkin_id = req.body.new_helper_checkin_id;

  yield db.helper_assignments.update(
    {helper_checkin_id: new_helper_checkin_id, close_time: null},
    {where: {id: helper_assignment_id}}
  );
  res.json({});
}


/* Resolve a helper assignment */
helper_assignments.destroy = function*(req, res) {
  var helper_assignment_id = req.params.helper_assignment;

  var result = yield db.helper_assignments.update(
    {close_time: db.sequelize.fn('NOW')},
    {where : {id: helper_assignment_id, close_time: null}}
  );

  res.json({});
}

module.exports = helper_assignments;
