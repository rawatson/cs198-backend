"use strict";
var db = require('../../models/index');

var global_state = {};

global_state.show = function*(req, res) {
  var attribute = req.params.global_state;
  var state = yield db.global_state.findOne({attributes: [attribute]}); 
  res.json({'data': state});
}

global_state.update = function*(req, res) {
  var attribute = req.params.global_state;
  var state = yield db.global_state.update(
    {lair_signups_enabled: req.body.lair_signups_enabled === true}, 
    {where: {}}
  );
  res.json({data: state});
}

module.exports = global_state;
