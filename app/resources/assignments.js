"use strict";
var db = require('../../models/index');

var assignments = {};

assignments.index = function*(req, res) {
  // Params: course_relation_id (good)
  var course_id = req.query.course_id;

  var assignments = yield db.assignments.findAll({
    where: {course_id: course_id}
  });

  res.json({data: assignments});
}

module.exports = assignments;
