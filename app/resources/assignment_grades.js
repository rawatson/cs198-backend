"use strict";
var db = require('../../models/index');

var assignment_grades = {};

assignment_grades.index = function*(req, res) {
  // Params: course_relation_id (good)
  var course_id  = req.query.course_id;

  // As of now, this will return all grades for the class
  // TODO: When we incorporate section data, we should filter to only include
  // the current SL's student's grades (where current sl comes from req)
  var assignment_grades = yield db.assignment_grades.findAll({
    include: {
      model: db.student_relations,
      where: {course_id: course_id},
      include: db.people
    }
  });

  res.json({data: assignment_grades});
}

module.exports = assignment_grades;
