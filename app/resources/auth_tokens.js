"use strict";
var db = require('../../models/index');
var auth = require('../auth.js');
var jwt = require('jwt-simple');

var auth_tokens = {};
// function to set validate the JWT as needed

// Set a users auth token into the cookie and redirect
auth_tokens.index = function*(req, res) {
  var token = req.query.token;
  var redirect_url = req.query.redirect_url;
  var decoded = jwt.decode(token, process.env.AUTH_SCRIPT_KEY);

  if (!decoded) {
    // Decode error -- this is either a bug or something mailicious
    console.log("decoding error");
  } else {
    var person = yield db.people.findOne({
      where: {sunetid: decoded.sunetid},
      attributes: ['id', 'sunetid', 'suid', 'first_name', 'last_name', 'nick_name', 'email', 'staff_status']
    });
    decoded = person.toJSON();
    console.log(decoded);
    token = jwt.encode(decoded, process.env.AUTH_SCRIPT_KEY);
  }

  var opts = {};

  // If we're running in production then require https
  if (process.env.NODE_ENV == 'production')
    opts.secure = true;
  res.cookie('jwt', token, opts);
  res.redirect(redirect_url);
}

//TODO: Remove this resource, since it's only used for testing
auth_tokens.protected_resources = {};
auth_tokens.protected_resources.index = function*(req, res) {
  console.log("called");
  console.log(auth)
  if (!(yield* auth.requireStaffRelation(req, res, 'Section Leader'))) {
    return;
  }
  res.json({});
};


module.exports = auth_tokens;
