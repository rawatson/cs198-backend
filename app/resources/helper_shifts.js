"use strict";
var db = require('../../models/index');

var helper_shifts = {};

function getShifts(person_id, before, after) {
  var where = {};
  if (before && after) {
    where.start_time = {$and: {$gte: after, $lte: before}};
  } else if (before) {
    where.start_time = {$lte: before};
  } else if (after) {
    where.start_time = {$gte: after};
  }

  if (person_id) {
    where.person_id = person_id;
  }

  return db.helper_shifts.findAll({
    where: where,
    include: db.people,
    order: ['start_time']
  });
}

helper_shifts.index = function*(req, res) {
  var before = undefined;
  var after = undefined;
  var person_id = undefined;

  if (req.query.before)
    before = new Date(req.query.before);
  if (req.query.after)
    after = new Date(req.query.after);
  if (req.query.sunetid)
    person_id = yield db.people.find({where: {sunetid: req.query.sunetid}});
  else if (req.query.person_id)
    person_id = parseInt(req.query.person_id);

  var helper_shifts = yield getShifts(person_id, before, after);

  res.send({'data': helper_shifts});
}

helper_shifts.update = function*(req, res) {
  console.log(req.params);
  console.log(req.body);
  var helper_shift_id = req.params.helper_shift;
  var helper_shift = yield db.helper_shifts.update(req.body, {
    where: {id: helper_shift_id},
    returning: true
  });
  res.json({data: helper_shift[0]});
}

function* convertNamesToIds(shifts) {
  var people = shifts.map(function(x) {return x.sunetid;});

  // Query the database for all the requested people
  var rows = yield db.people.findAll({
    attributes: ['sunetid', 'id'],
    where: {sunetid: {$in: people}},
    raw: true
  })

  // Then, turn the rows into a sunet => id mapping
  var mapping = {};
  for (var row of rows) {
    mapping[row.sunetid] = row.id;
  }
  
  // Then, return the shifts with their new person_ids
  return shifts.map(function(shift) {
    // Make sure the person we're looking for exists
    if (!(shift.sunetid in mapping)) {
      throw `Cannot locate a person with sunetid ${shift.sunetid}`;
    }
    
    // Also check that the start time is valid before we insert anything
    if (new Date(shift.start_time) < new Date()) {
      throw `Cannot create a shift at ${shift.start_time} (in the past)`
    }

    shift.person_id = mapping[shift.sunetid];
    delete shift.sunetid;
    return shift;
  });
}

helper_shifts.create = function*(req, res) {
  var shifts = req.body.shifts;

  // Get the DB ids of our helpers
  shifts = yield* convertNamesToIds(shifts);

  var results = [];
  for (var shift of shifts) {
    for (var i = 0; i < shift.repeat; ++i) {
      var opts = {
        person_id: shift.person_id,
        start_time: new Date(shift.start_time),
        duration_minutes: shift.duration_minutes
      };
      // Increment by one week as needed
      opts.start_time.setDate(opts.start_time.getDate() + 7 * i);

      results.push(yield db.helper_shifts.create(opts, {returning: true}));
    }
  }
  res.json({data: results});
}

function* handleBulkDelete(req, res) {
  var person_id = req.body.person_id;
  if (!person_id) {
    throw `Cannot delete all future shifts for person id '${person_id}'`;
  }

  var deleted_row_count = yield db.helper_shifts.destroy({
    where: {
      person_id: person_id,
      start_time: {$gte: new Date()},
      regular_shift: true
    }
  });

  res.json({data: `Successfully deleted ${deleted_row_count} shifts`});
}

helper_shifts.destroy = function*(req, res) {
  var helper_shift_id = req.params.helper_shift;
  if (helper_shift_id === 'all_future_shifts') {
    return yield* handleBulkDelete(req, res);
  }

  var deleted_row_count = yield db.helper_shifts.destroy({
    where: {
      id: helper_shift_id
    }
  });

  res.json({data: `Successfully deleted ${deleted_row_count} shifts`});
}

helper_shifts.swap = {};
helper_shifts.swap.create = function*(req, res) {
  var id_1 = req.body.helper_shift_id_1,
      id_2 = req.body.helper_shift_id_2;

  var rows = yield db.sequelize.query(`
    SELECT id, person_id
    FROM helper_shifts
    WHERE id IN (?, ?)`, {replacements: [id_1, id_2]});
  rows = rows[0];

  yield [
    db.helper_shifts.update({person_id: rows[0].person_id, regular_shift: false},
      {where: {id: rows[1].id}}),
    db.helper_shifts.update({person_id: rows[1].person_id, regular_shift: false},
      {where: {id: rows[0].id}}),
  ];

  res.json({data: 'Swap successful!'});
}

module.exports = helper_shifts;
