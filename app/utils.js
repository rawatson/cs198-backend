var co = require('co');
var express = require('express');
var pluralize = require('pluralize');

var fnMapping = {
  index: function(app, resourceName, fn, prefix) {
    var url = `${prefix}${resourceName}/?`;
    console.log(url);
    app.get(url, fn);
  },
  create: function(app, resourceName, fn, prefix) {
    var url = `${prefix}${resourceName}/?`
    app.post(url, fn);
  }, 
  show: function(app, resourceName, fn, prefix) {
    var url = `${prefix}${resourceName}/:${pluralize(resourceName, 1)}`
    app.get(url, fn);
  },
  update: function(app, resourceName, fn, prefix) {
    var url = `${prefix}${resourceName}/:${pluralize(resourceName, 1)}`
    app.put(url, fn);
  },
  destroy: function(app, resourceName, fn, prefix) {
    var url = `${prefix}${resourceName}/:${pluralize(resourceName, 1)}`
    app.delete(url, fn);
  }
}

function createEndpoint(app, resourceName, method, handler, prefix) {
  var endpoint = handler[method];
  var endpointFn = function(req, res) {
    co(function*() { yield* endpoint(req, res); }).catch(function(err) {
      console.log(err);
      if (typeof err === 'string') {
        res.json({error: err});
      } else {
        console.log(err.stack);
        res.send(err);
      }
    });
  };
  fnMapping[method](app, resourceName, endpointFn, prefix);
}

// TODO: refactor this code, it's a bit of a mess now
var app = express.application;
app.cs198_resource = function(resourceName, handler) {
  for (var method of Object.keys(handler)) {
    if (method in fnMapping) {
      createEndpoint(this, resourceName, method, handler, '/');
    } else {
      if (typeof(handler[method]) !== 'object') {
        continue;
      }
      var prefix = `/${resourceName}/:${pluralize(resourceName, 1)}/`;
      for (var meth of Object.keys(handler[method])) {
        if (meth in fnMapping) {
          createEndpoint(this, method, meth, handler[method], prefix);
        }
      }
    }
  }
}
