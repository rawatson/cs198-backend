var nodemailer = require('nodemailer');
var moment = require('moment');
var db = require('../../models/index');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASSWORD
  }
}, {
  from: 'CS198 Coordinators <cs198@cs.stanford.edu>' 
});

module.exports = function () {
  db.sequelize.query(`
    SELECT
      sunetid, first_name, nick_name, start_time
    FROM
      (SELECT *
       FROM helper_shifts
       WHERE start_time < now()
       AND start_time + (duration_minutes || 'minutes')::interval > now()) AS ongoing_shifts
    LEFT JOIN
      (SELECT *
       FROM helper_checkins
       WHERE check_out_time IS null) AS ongoing_checkins ON ongoing_shifts.person_id = ongoing_checkins.person_id
    INNER JOIN people ON people.id = ongoing_shifts.person_id
    WHERE check_in_time IS null
    `, {type: db.sequelize.QueryTypes.SELECT}).then(function (people) {

    // data is of the format [ { sunetid: string, first_name: string, nick_name: string, start_time: Date} ]
    // nick_name is null if no nickname exists for user
    for (var i = 0; i < people.length; ++i) {
      var person = people[i];
      var shift_date = moment(person.start_time).format('MMMM D, YYYY'); // 'January 1st, 2016'
      var shift_time = moment(person.start_time).format('h:mm A'); // 1:44 AM

      var mailOptions = {
        to: `${ person.sunetid }@stanford.edu`,
        bcc: 'cs198coords+lairreminders@gmail.com',
        subject: `LaIR Shift at ${ shift_time }`,
        text: `Dear ${ (person.nick_name) ? person.nick_name : person.first_name },

This email is to remind you that you have a LaIR shift today, ${ shift_date }, at ${ shift_time }.

---
Aaron, Danielle, and Greg
CS198 Coordinators`
      };

      transporter.sendMail(mailOptions)
      .catch(function (err) {
        console.log('Problem sending email: ' + err)
      });
    }
  });
}