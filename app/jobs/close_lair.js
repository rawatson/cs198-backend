var db = require('../../models/index');

module.exports = function () {
  db.helper_assignments.update(
    { close_time: new Date() },
    { where: { close_time: {$eq: null} } }
  ).then(function () {
    db.helper_checkins.update(
      { check_out_time: new Date() },
      { where: { check_out_time: {$eq: null} } }
    )
  });
}