var express = require('express');
var cors = require('cors')
var bodyParser = require('body-parser');
var compression = require('compression')
var cookieParser = require('cookie-parser')
var mysql = require('mysql-co');
var CronJob = require('cron').CronJob;

require('dotenv').config({path: __dirname + '/../.env'});
require('./utils.js');

var mysqlcs = {
  "host": process.env.LEGACY_DB_HOST,
  "user": process.env.LEGACY_DB_USER,
  "password": process.env.LEGACY_DB_PASSWORD,
  "database": process.env.LEGACY_DB_DATABASE
};
if (process.env.LEGACY_DB_HOST) {
  //express.application.legacy_db = mysql.createConnection(mysqlcs);
} else {
  express.application.legacy_db = null;
}

new CronJob('0 5 18-22 * * 0-4', require('./jobs/lair_reminder.js')).start();
new CronJob('0 0 1 * * 1-5', require('./jobs/close_lair.js')).start();

var app = express();
app.set('json spaces', 4);

app.use(cors({origin:true, credentials:true}));
app.use(bodyParser.json());
app.use(compression());
app.use(cookieParser());

// Set up legacy DB



app.cs198_resource('people', require('./resources/people'));
app.cs198_resource('helper_checkins', require('./resources/helper_checkins'));
app.cs198_resource('helper_assignments', require('./resources/helper_assignments'));
app.cs198_resource('helper_shifts', require('./resources/helper_shifts'));
app.cs198_resource('global_state', require('./resources/global_state'));
app.cs198_resource('help_requests', require('./resources/help_requests'));
app.cs198_resource('auth_tokens', require('./resources/auth_tokens'));
app.cs198_resource('assignments', require('./resources/assignments'));
app.cs198_resource('assignment_grades', require('./resources/assignment_grades'));

// Add a json based 404 handler
app.use(function(req, res) {
  res.status(404);
  var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

  res.send({error: `404 not found (${fullUrl})`});
});

app.listen(3000, function() {
  console.log("Listening for requests on port 3000");  
});
