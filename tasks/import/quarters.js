"use strict";
var co = require('co');
var quarter_tools = require('./_quarter_tools')
function migrateQuarters(pg, mysql) {
  return co(function*() {
    var mysql_result = yield mysql.query('SELECT DISTINCT ID, Year, Quarter FROM Quarters');
    var psql_queries = [];
    var quarter_names = ['Winter', 'Spring', 'Summer', 'Autumn'];
    var query = "INSERT INTO Quarters (id, year, quarter_name) VALUES ($1, $2, $3)";
    for (var row of mysql_result[0]) {
      var id = quarter_tools.translateID(row.Year, row.Quarter);
      var values = [id, row.Year, quarter_names[row.Quarter - 1]];
      psql_queries.push(pg.queryPromise(query, values));
    }
    yield psql_queries;
    console.log("Quarter migrations completed");
  });
}
module.exports.migrate = migrateQuarters;
