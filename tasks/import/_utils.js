"use strict";
var co = require('co');
var quarter_tools = require('./_quarter_tools')
function cleanDatabase(pg) {
  return co(function*() {
    yield pg.queryPromise('DELETE FROM paperless_comments');
    yield pg.queryPromise('DELETE FROM assignment_grades');
    yield pg.queryPromise('DELETE FROM submissions');
    yield pg.queryPromise('DELETE FROM assignments');

    yield pg.queryPromise('DELETE FROM global_state');

    yield pg.queryPromise('DELETE FROM helper_shifts');
    yield pg.queryPromise('DELETE FROM helper_assignments');
    yield pg.queryPromise('DELETE FROM helper_checkins');
    yield pg.queryPromise('DELETE FROM help_requests');

    yield pg.queryPromise('DELETE FROM student_relations');
    yield pg.queryPromise('DELETE FROM staff_relations');
    yield pg.queryPromise('DELETE FROM group_relations');

    yield pg.queryPromise('DELETE FROM courses');
    yield pg.queryPromise('DELETE FROM people');
    yield pg.queryPromise('DELETE FROM quarters');
    console.log("All existing records cleared");
  });
}
module.exports.cleanDatabase = cleanDatabase;
