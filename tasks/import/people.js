"use strict";
var co = require('co');
var quarter_tools = require('./_quarter_tools')
function migratePeople(pg, mysql) {
  var translateGender = function(gender) {
     if (gender === 'M') return 'Male';
     else if (gender === 'F') return 'Female';
     else if (gender === 'O') return 'Other';
     else return 'Unknown';
  };
  var translateCitizenStatus = function(citizenStatus) {
    if (citizenStatus === 1) return 'US Citizen';
    else if (citizenStatus === 2) return 'Permanent Resident';
    else if (citizenStatus === 3) return 'International';
    else return 'Unknown';
  }

  // A note: we have 200 people in the old database with profiles but no SUID.
  // We'd really like to make SUID a mandatory field, so we're adding a NOT
  // NULL check to the field.  To import the legacy data, we're giving these
  // individuals SUID 0 + i.  We SHOULD give them NULL SUIDs, but that would mean
  // that data imported in the future could fail the NOT NULL check.  In effect
  // we're grandfathering in anyone with no SUID by giving them SUID 0 + i
  var suid_counter = 0;
  var translateSUID = function(suid) {
    if (suid !== null) return suid;
    else return suid_counter++;
  }

  var translateStaffStatus = function(staffStatus) {
    if (staffStatus == 1) return 'Active';
    else if (staffStatus == 2) return 'Inactive / Alumni';
    else return 'Non-Staff';
  }

  var createPeopleQueryPromise = function(p) {
    var hireQuarter = -1;
    if (p.HireDate == null)
      hireQuarter = null;
    else
      hireQuarter = quarter_tools.translateID(p.Year, p.Quarter);


    var query = `INSERT INTO people (id, suid, sunetid, first_name, last_name,
    nick_name, email, phone_number, gender, citizen_status, staff_status, hire_quarter_id)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`;

    var values = [p.ID, translateSUID(p.SUID), p.SUNetID, p.FirstName, p.LastName, p.NickName,
              p.Email, p.PhoneNumber, translateGender(p.Gender),
              translateCitizenStatus(p.CitizenStatus),
              translateStaffStatus(p.StaffStatus), hireQuarter];
    return pg.queryPromise(query, values);
  }

  return co(function*() {
    var mysql_result = yield mysql.query(`
      SELECT People.*, Quarters.Year, Quarters.Quarter
      FROM People LEFT JOIN Quarters ON People.HireDate = Quarters.ID`);
    var psql_queries = []

    for (var p of mysql_result[0]) {
      var peopleQP = createPeopleQueryPromise(p);
      if (peopleQP !== null)
        psql_queries.push(peopleQP);
   }
    yield psql_queries
    console.log('People migrations completed');
  });
}
module.exports.migrate = migratePeople;
