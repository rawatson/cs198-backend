"use strict";
var co = require('co');
function seed(pg) {
  var getCourseRelationPromise = function(sunetid, course_number) {
    return pg.queryPromise(`
      SELECT student_relations.id
      FROM student_relations
        INNER JOIN people ON student_relations.person_id = people.id
        INNER JOIN courses ON student_relations.course_id = courses.id
      WHERE
        People.sunetid = '${sunetid}'
        AND courses.course_number ='${course_number}'
    `).then(function(res) {
      return res.rows[0].id;   
    });;
  }

  /*
   *
   * Seeding functions
   *
   */

  var seedAssignments = function*(current_quarter_id) {
    var currentQuarterCS106B = yield pg.queryPromise(`
      SELECT courses.id
      FROM courses
      WHERE courses.quarter_id = ${current_quarter_id}
      AND courses.course_number='106b'
    `).then(function(res) {
      return res.rows[0].id;
    });

    var createAssignment = function(assignment_name, assignment_number) {
      return pg.queryPromise(`
        INSERT INTO assignments (
          course_id, due_date, assignment_name, assignment_number
        ) VALUES (
          ${currentQuarterCS106B},
          NOW() - interval '${5 - assignment_number} week',
          '${assignment_name}',
          ${assignment_number}
        )
      `);
    }

    // Common CS106B assignments
    yield [
      createAssignment("Game of Life", 1),
      createAssignment("ADTs", 2),
      createAssignment("Recursion", 3),
      createAssignment("Boggle", 4),
      createAssignment("PQueue", 5)
    ]
  };

  var seedStudents = function*() {
    var createPerson = function(suid, sunetid, first_name, last_name) {
      return pg.queryPromise(`
        INSERT INTO people (
          suid, sunetid, first_name, last_name, email
        ) VALUES (
          '${suid}',
          '${sunetid}',
          '${first_name}',
          '${last_name}',
          '${sunetid}@stanford.edu'
        ) RETURNING *
      `);
    }

    return Promise.all([
      createPerson(99999999001, 'axkjqqza', 'John', 'Doe'),
      createPerson(99999999902, 'axkjqqzb', 'Jane', 'Doe'),
      createPerson(99999999903, 'axkjqqzc', 'Bruce', 'Wayne'),
      createPerson(99999999904, 'axkjqqzd', 'Martha', 'Kent')
    ]).then(function(all_res) {
      return all_res.map(function(query_res) {
        return query_res.rows[0];
      });
    });
  }

  var seedLair = function*(current_quarter_id) {
    var kevin_106a_student_relation_id = yield getCourseRelationPromise('hgkshin', '106a');
    var nhien_106b_student_relation_id = yield getCourseRelationPromise('nhient', '106b');
    var res = yield [
      pg.queryPromise(`INSERT INTO helper_checkins (person_id) SELECT id FROM people WHERE sunetid = 'rawatson' RETURNING id`),
      pg.queryPromise(`INSERT INTO global_state (lair_signups_enabled, current_quarter_id) VALUES (true, ${current_quarter_id})`),
      pg.queryPromise(`INSERT INTO help_requests (student_relation_id, problem_description, location)
                       VALUES (
                         ${kevin_106a_student_relation_id}, 
                         'I am Kevin and I have problems',
                         'Computer 33 (kevin)') RETURNING id`),
      pg.queryPromise(`INSERT INTO help_requests (student_relation_id, problem_description, location)
                       VALUES (
                         ${nhien_106b_student_relation_id}, 
                         'I am Nhien and I have problems',
                         'Computer 40 (nhien)') RETURNING id`)
 
    ];
    var reid_helper_checkin_id = res[0].rows[0].id;
    var kevin_help_request_id = res[2].rows[0].id;
    yield pg.queryPromise(`INSERT INTO helper_assignments (
        helper_checkin_id, help_request_id
        ) VALUES (
          ${reid_helper_checkin_id}, ${kevin_help_request_id}
        )`);
  }

  var seedAssignmentSubmissions = function*(seed_sl_sunetid, current_quarter_id, student_people) {
    yield pg.queryPromise(`
      INSERT INTO staff_relations
        (person_id, course_id, position)
      SELECT
        people.id,
        courses.id,
        'Section Leader'
      FROM
        people
        cross join courses
      WHERE
        people.sunetid = '${seed_sl_sunetid}'
        AND courses.course_number='106b'
        AND courses.quarter_id=${current_quarter_id}
    `);
  }


  var seedSection = function*(seed_sl_sunetid, current_quarter_id, student_people) {
    yield pg.queryPromise(`
      INSERT INTO staff_relations
        (person_id, course_id, position)
      SELECT
        people.id,
        courses.id,
        'Section Leader'
      FROM
        people
        cross join courses
      WHERE
        people.sunetid = '${seed_sl_sunetid}'
        AND courses.course_number='106b'
        AND courses.quarter_id=${current_quarter_id}
    `);
  }

  return co(function*() {
    console.log('Seeding data');

    var seed_sl_sunetid = 'rawatson'; // TODO: prompt for this?
    var current_quarter_id = yield pg.queryPromise(`SELECT MAX(id) AS id FROM quarters`)
    .then(function(res) {
      return res.rows[0].id
    });

    var student_people= yield* seedStudents();
    yield* seedSection(seed_sl_sunetid, current_quarter_id, student_people); 
    yield* seedAssignments(current_quarter_id);
    yield* seedAssignmentSubmissions(seed_sl_sunetid, current_quarter_id, student_people);
    yield* seedLair(current_quarter_id);
  });
}
module.exports.migrate = seed;
