"use strict";
var co = require('co');
var quarter_tools = require('./_quarter_tools')
function migratePeople(pg, mysql) {
  var inserts = 0, updates = 0;


  var translateGender = function(gender) {
     if (gender === 'M') return 'Male';
     else if (gender === 'F') return 'Female';
     else if (gender === 'O') return 'Other';
     else return 'Unknown';
  };

  var translateCitizenStatus = function(citizenStatus) {
    if (citizenStatus === 1) return 'US Citizen';
    else if (citizenStatus === 2) return 'Permanent Resident';
    else if (citizenStatus === 3) return 'International';
    else return 'Unknown';
  }

  // A note: we have 200 people in the old database with profiles but no SUID.
  // We'd really like to make SUID a mandatory field, so we're adding a NOT
  // NULL check to the field.  To import the legacy data, we're giving these
  // individuals SUID 0 + i.  We SHOULD give them NULL SUIDs, but that would mean
  // that data imported in the future could fail the NOT NULL check.  In effect
  // we're grandfathering in anyone with no SUID by giving them SUID 0 + i
  var suid_counter;

  // When syncing, we need to keep track of what the last "fake" suid we used was
  var initializeSUIDCounter = function() {
    return pg.queryPromise(`
      SELECT MAX(suid) AS initial_suid
      FROM  people
      WHERE suid < 10000`)
    .then(function(res) {
      if (res.rows.length == 0) {
        suid_counter = 0;
      } else {
        suid_counter = res.rows[0].initial_suid + 1
      }
    });
  }

  var translateSUID = function(suid) {
    if (suid !== null) return suid;
    else return suid_counter++;
  }

  var translateStaffStatus = function(staffStatus) {
    if (staffStatus == 1) return 'Active';
    else if (staffStatus == 2) return 'Inactive / Alumni';
    else return 'Non-Staff';
  }

  var getImportedIds = function*(mysql_rows) {
    return pg.queryPromise(`SELECT id FROM people`)
    .then(function(res) {
      var set = new Set();
      for (var row of res.rows) {
        set.add(parseInt(row.id));
      }
      return set;
    });
  }

  var createPeopleQueryPromise = function(p, importedIds) {
    var hireQuarter = -1;
    if (p.HireDate == null)
      hireQuarter = null;
    else
      hireQuarter = quarter_tools.translateID(p.Year, p.Quarter);


    var query;
    var values;
    // when we get pg 9.5 please use upsert instead
    if (!importedIds.has(p.ID)) {
      inserts += 1;
      query = `INSERT INTO people (id, suid, sunetid, first_name, last_name,
      nick_name, email, phone_number, gender, citizen_status, staff_status, hire_quarter_id)
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`;

      values = [p.ID, translateSUID(p.SUID), p.SUNetID, p.FirstName, p.LastName, p.NickName,
                p.Email, p.PhoneNumber, translateGender(p.Gender),
                translateCitizenStatus(p.CitizenStatus),
                translateStaffStatus(p.StaffStatus), hireQuarter];
    } else {
      updates += 1;
      query = `UPDATE people 
        SET suid=$1,
            sunetid=$2,
            first_name = $3,
            last_name = $4,
            nick_name = $5,
            email = $6,
            phone_number = $7,
            gender = $8,
            citizen_status = $9,
            staff_status = $10,
            hire_quarter_id = $11
        WHERE id=$12`;

      values = [translateSUID(p.SUID), p.SUNetID, p.FirstName, p.LastName, p.NickName,
                p.Email, p.PhoneNumber, translateGender(p.Gender),
                translateCitizenStatus(p.CitizenStatus),
                translateStaffStatus(p.StaffStatus), hireQuarter, p.ID];
    }

    return pg.queryPromise(query, values);
  }

  return co(function*() {
    console.log("People sync starting");
    var mysql_result = yield mysql.query(`
      SELECT People.*, Quarters.Year, Quarters.Quarter
      FROM People LEFT JOIN Quarters ON People.HireDate = Quarters.ID`);
    var psql_queries = []

    var importedIds = yield getImportedIds();
    yield initializeSUIDCounter();
    for (var p of mysql_result[0]) {
      var peopleQP = createPeopleQueryPromise(p, importedIds);
      if (peopleQP !== null)
        psql_queries.push(peopleQP);
    }
    yield psql_queries

    console.log(`  Imported ${inserts} people`);
    console.log(`  Updated ${updates} people`);
    console.log('People sync completed');
  });
}
module.exports.sync = migratePeople;
