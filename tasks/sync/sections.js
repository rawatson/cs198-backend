"use strict";
var co = require('co');
var quarter_tools = require('./_quarter_tools')
function migrateCourseRelations(pg, mysql) {

  var cachedCourses = {};
  var getCourseCacheKeyPSQL = function(c) {
    var key = "";
    key += c.department_code;
    key += ":";
    key += c.course_number;
    key += ":";
    key += c.quarter_id;
    return key;
  }

  var createCoursesCache = function() {
    return pg.queryPromise(`
      SELECT
        courses.id, courses.department_code, courses.course_number, courses.quarter_id
      FROM 
        courses`)
    .then(function(courses) {
      for (var c of courses.rows) {
        cachedCourses[getCourseCacheKeyPSQL(c)] = c.id;
      }
    });
  }
  var populateCourseFields = function(c) {
      var codeTokens = c.RegistryDocName.split('-');
      c.DepartmentCode = codeTokens[0];
      c.CourseNumber = codeTokens[1];
  }
  var getCourseCacheKeyMySQL = function(c) {
    populateCourseFields(c);
    var key = "";
    key += c.DepartmentCode;
    key += ":";
    key += c.CourseNumber;
    key += ":";
    key += quarter_tools.translateID(c.Year, c.QuarterNumber);
    return key;
  }




  var getImportedIds = function() {
    return pg.queryPromise(`SELECT id FROM sections`)
    .then(function(res) {
      var set = new Set();
      for (var row of res.rows) {
        set.add(parseInt(row.id));
      }
      return set;
    });
  }

  var createSectionQueryPromise = function(c, importedIds) {
    var course_id = cachedCourses[getCourseCacheKeyMySQL(c)];
    var query, values;
    if (!importedIds.has(c.ID)) {
      query = `
      INSERT INTO
        sections (id, course_id, section_leader_id)
      VALUES
        ($1, $2, $3)
      `;
      values = [c.ID, course_id, c.SectionLeader];
    } else {
      query = `
      UPDATE sections SET
        course_id = $1,
        section_leader_id = $2
      WHERE
        id = $3
      `;
      values = [course_id, c.SectionLeader, c.ID];
    }
    if (!values || !query) {
      console.log(query);
      console.log(values);
    }
    return pg.queryPromise(query, values);
  }
  return co(function*() {
    var query_string =`
      SELECT
        Sections.ID,
        Sections.Quarter,
        Sections.SectionLeader,
        Sections.Class,
        Quarters.Year,
        Quarters.Quarter AS QuarterNumber,
        Courses.RegistryDocName
      FROM
        Sections
        INNER JOIN Quarters ON Sections.Quarter = Quarters.ID
        INNER JOIN Courses ON Sections.Class = Courses.ID
      `;
    var mysql_result = yield mysql.query(query_string);
    var importedIds = yield getImportedIds();

    yield createCoursesCache();
    var psql_queries = [];
    for (var p of mysql_result[0]) {
      populateCourseFields(p)

      var courseQP = createSectionQueryPromise(p, importedIds);
      if (courseQP !== null)
        psql_queries.push(courseQP);
    }
    yield psql_queries;

    console.log("Sections sync completed");
  });
}
module.exports.sync = migrateCourseRelations;
