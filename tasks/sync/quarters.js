"use strict";
var co = require('co');
var quarter_tools = require('./_quarter_tools')
function migrateQuarters(pg, mysql) {

  var getImportedIds = function*(mysql_rows) {
    return pg.queryPromise(`SELECT id FROM quarters`)
    .then(function(res) {
      var set = new Set();
      for (var row of res.rows) {
        set.add(row.id);
      }
      return set;
    });
  }

  return co(function*() {
    var mysql_result = yield mysql.query('SELECT DISTINCT ID, Year, Quarter FROM Quarters');
    var mysql_rows = mysql_result[0];

    var quarter_names = ['Winter', 'Spring', 'Summer', 'Autumn'];
    var query = "INSERT INTO Quarters (id, year, quarter_name) VALUES ($1, $2, $3)";

    var importedIds = yield getImportedIds(mysql_rows);
    var psql_queries = [];
    for (var row of mysql_result[0]) {
      var id = quarter_tools.translateID(row.Year, row.Quarter);
      // Don't reinsert rows we already have
      if (importedIds.has(id))
        continue;

      var values = [id, row.Year, quarter_names[row.Quarter - 1]];
      psql_queries.push(pg.queryPromise(query, values));
    }
    yield psql_queries;

    console.log("Quarter migrations completed");
  });
}
module.exports.sync = migrateQuarters;
