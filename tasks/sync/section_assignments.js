"use strict";
var co = require('co');
var quarter_tools = require('./_quarter_tools')
function sync(pg, mysql) {
  var updates = 0, inserts = 0, deletes = 0;

  var getImportedIds = function() {
    return pg.queryPromise(`SELECT id FROM section_assignments`)
    .then(function(res) {
      var set = new Set();
      for (var row of res.rows) {
        set.add(parseInt(row.id));
      }
      return set;
    });
  }


  var createSectionAssignmentQueryPromise = function(c, idsToUpdate) {
    var quarter = quarter_tools.translateID(c.Year, c.QuarterNumber);

    var query, values;
    if (idsToUpdate.has(c.ID)) {
      updates += 1;
      idsToUpdate.delete(c.ID);
      query = `
        UPDATE section_assignments SET
          section_id = $1,
          student_id = $2
        WHERE
          id = $3
      `;
      values = [c.Section, c.Person, c.ID];
    } else {
      inserts += 1;
      query =`
        INSERT INTO section_assignments (id, section_id, student_id)
        VALUES ($1, $2, $3)
      `;
      values = [c.ID, c.Section, c.Person];
    }
    return pg.queryPromise(query, values);
  }
  var cleanDeleted = function(idsToDelete) {
    var psql_queries = [];
    for (var id of idsToDelete) {
      deletes += 1;
      var qp = pg.queryPromise(`
        DELETE FROM section_assignments
        WHERE id = $1`, [id]);
      psql_queries.push(qp);
    }
    return psql_queries;
  }

  return co(function*() {
    var query_string =
      `SELECT
        SectionAssignments.ID,
        SectionAssignments.Person,
        SectionAssignments.Section
      FROM
        SectionAssignments
      `;

    var importedIds = yield getImportedIds();
    var mysql_result = yield mysql.query(query_string);
    var psql_queries = [];
    for (var p of mysql_result[0]) {
      var qp = createSectionAssignmentQueryPromise(p, importedIds);
      if (qp !== null)
        psql_queries.push(qp);
    }
    yield psql_queries;

    // Now we need to clean out any section assignments which were deleted in the legacy db
    yield cleanDeleted(importedIds);

    console.log(`  Inserted ${inserts} records`);
    console.log(`  Updated ${updates} records`);
    console.log(`  Deleted ${deletes} records`);
    console.log("Courses sync completed");
  });
}
module.exports.sync = sync;
