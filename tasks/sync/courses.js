"use strict";
var co = require('co');
var quarter_tools = require('./_quarter_tools')
function migrateCourseRelations(pg, mysql) {

  var createCourseQueryPromise = function(c) {
    // We use a cute little hack to do INSERT ON CONFLICT IGNORE
    // It's got a concurrency bug but we have a unique check so it shouldn't be
    // a problem
    var quarter = quarter_tools.translateID(c.Year, c.QuarterNumber);
    var query = `
    INSERT INTO
      courses (department_code, course_number, quarter_id)
    SELECT
      $1, $2, $3
    WHERE NOT EXISTS
    ( SELECT department_code FROM courses
      WHERE department_code = $4 AND course_number = $5 AND quarter_id = $6
    )`;
    var values = [c.DepartmentCode, c.CourseNumber, quarter, c.DepartmentCode, c.CourseNumber, quarter];
    return pg.queryPromise(query, values);
  }

  var populateCourseFields = function(c) {
      var codeTokens = c.RegistryDocName.split('-');
      c.DepartmentCode = codeTokens[0];
      c.CourseNumber = codeTokens[1];
  }

  return co(function*() {
    var query_string =
      `SELECT DISTINCT
        Courses.*,
        CourseRelations.Quarter,
        Quarters.Year,
        Quarters.Quarter AS QuarterNumber
      FROM
        Courses
      INNER JOIN
        CourseRelations ON Courses.ID = CourseRelations.Class
      INNER JOIN
        Quarters on CourseRelations.Quarter = Quarters.ID
      `;
    var mysql_result = yield mysql.query(query_string);
    var psql_queries = [];
    for (var p of mysql_result[0]) {
      populateCourseFields(p);

      var courseQP = createCourseQueryPromise(p);
      if (courseQP !== null)
        psql_queries.push(courseQP);
    }
    yield psql_queries;

    console.log("Courses sync completed");
  });
}
module.exports.sync = migrateCourseRelations;
