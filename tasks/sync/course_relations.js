"use strict";
var co = require('co');
var quarter_tools = require('./_quarter_tools')
function migrateCourseRelations(pg, mysql) {
  var relationsIgnored = 0, relationsAdded = 0;

  // cachedCourses is the same as cachedGroups, but with
  // department_code:course_number:quarter_id strings as the key and course ids
  // as values
  var cachedCourses = {}

  var getCourseCacheKeyMySQL = function(c) {
    populateCourseFields(c);
    var key = "";
    key += c.DepartmentCode;
    key += ":";
    key += c.CourseNumber;
    key += ":";
    key += quarter_tools.translateID(c.Year, c.QuarterNumber);
    return key;
  }

  var getCourseCacheKeyPSQL = function(c) {
    var key = "";
    key += c.department_code;
    key += ":";
    key += c.course_number;
    key += ":";
    key += c.quarter_id;
    return key;
  }

  var createCoursesCache = function() {
    return pg.queryPromise(`
      SELECT
        courses.id, courses.department_code, courses.course_number, courses.quarter_id
      FROM 
        courses`)
    .then(function(courses) {
      for (var c of courses.rows) {
        cachedCourses[getCourseCacheKeyPSQL(c)] = c.id;
      }
    });
  }

  var populateCourseFields = function(c) {
    var codeTokens = c.RegistryDocName.split('-');
    c.DepartmentCode = codeTokens[0];
    c.CourseNumber = codeTokens[1];
  }


  var translatePosition = function(c) {
    if (c.Position == 1) return 'Student';
    else if (c.Position == 2) return 'Applicant';
    else if (c.Position == 3) return 'Course Helper';
    else if (c.Position == 4) return 'Section Leader';
    else if (c.Position == 5) return 'Head TA';
    else if (c.Position == 6) return 'Lecturer';
    else if (c.Position == 7) return 'Coordinator';
    else return 'Error';
  }

  var createGroupMemberQueryPromise = function(c) {
    var query = "";
    var values = [];
    if (c.Position == 1) {
      query = `
      INSERT INTO
        student_relations (course_id, person_id, listed_in_axess)
      SELECT
        $1, $2, $3
      WHERE NOT EXISTS
      ( SELECT 1 FROM student_relations
        WHERE course_id = $1 AND person_id = $2
      )`;
      values = [cachedCourses[getCourseCacheKeyMySQL(c)], c.Person, c.InRegistry == 1];
    } else if (c.Position == 2) {
      query =`
      INSERT INTO
        group_relations (group_name, course_id, person_id)
      SELECT
        'Applicant', $1, $2
      WHERE NOT EXISTS
      ( SELECT 1 FROM group_relations
        WHERE group_name='Applicant' AND course_id=$1 AND person_id=$2
      )`;
      values = [cachedCourses[getCourseCacheKeyMySQL(c)], c.Person];
    } else if (c.Position >= 3 && c.Position <= 7) {
      query =`
      INSERT INTO
        staff_relations (course_id, person_id, position)
      SELECT
        $1, $2, $3
      WHERE NOT EXISTS
      ( SELECT 1 FROM staff_relations
        WHERE course_id=$1 AND person_id =$2 AND position=$3
      )`;
      var position = translatePosition(c);
      if (!cachedCourses[getCourseCacheKeyMySQL(c)]) {
        console.log(getCourseCacheKeyMySQL(c));
        return;
      }
      values = [cachedCourses[getCourseCacheKeyMySQL(c)], c.Person, position];
    } else {
      query = "SELECT missing_thing FROM invalid_table"
    }

    return pg.queryPromise(query, values).then(function(res) {
      if (res.rowCount == 0)
        relationsIgnored += 1;
      else
        relationsAdded += 1;
    });
  }


  return co(function*() {
    var mysql_result = yield mysql.query(`
      SELECT CourseRelations.*, Courses.*, Quarters.Year, Quarters.Quarter AS QuarterNumber
      FROM CourseRelations
      INNER JOIN Courses on Courses.ID = CourseRelations.Class
      INNER JOIN Quarters ON CourseRelations.Quarter = Quarters.ID
    `);

    // Initialization: build a cache of our courses
    yield createCoursesCache();

    // Finally: translate CourseRelations into enrollments and group memberships
    var psql_queries = []
    for (var c of mysql_result[0]) {
      populateCourseFields(c);
      psql_queries.push(createGroupMemberQueryPromise(c));
    }

    yield psql_queries;
    console.log(`  Imported ${relationsAdded} course relations`);
    console.log(`  Already had ${relationsIgnored} course relations`);
    console.log('CourseRelations migrations completed');
  });
}
module.exports.sync = migrateCourseRelations;
