"use strict";
// Input is a year and mysql quarter number.  Mysql quarter numbers look like this: 
// 1: Winter
// 2: Spring
// 3: Summer
// 4: Autumn
//
// This function translates to peoplesoft code IDs, e.g. 1154 for win2015.
// First 3 digits are years since 1900
// Last 1 digit is the quarter:
// 2: Autumn
// 4: Winter
// 6: Spring
// 8: Summer
var translateID = function(year, mysql_quarter) {
  // Translate mysql calendar years into peoplesoft academic years
  var quarter_digit = 0;
  if (mysql_quarter == 1) {
    quarter_digit = 4;
  } else if (mysql_quarter == 2) {
    quarter_digit = 6;
  } else if (mysql_quarter == 3) {
    quarter_digit = 8;
  } else if (mysql_quarter == 4) {
    quarter_digit = 2;
    year += 1;
  } else {
    return -1000000;
  }
  return 10 * (year - 1900) + quarter_digit;
}
module.exports.translateID = translateID
 
