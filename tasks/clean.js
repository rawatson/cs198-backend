"use strict";

require('dotenv').config({path: __dirname + '/../.env'});
var co = require('co');

co(function*() {
  var pgcs = process.env.DATABASE_URL;
  var pg = require('co-pg')(require('pg'));
  var results = yield pg.connectPromise(pgcs);
  var client = results[0], done = results[1];

  console.log("Clearing all database records");
  yield client.queryPromise('DELETE FROM section_assignments');
  yield client.queryPromise('DELETE FROM sections');

  yield client.queryPromise('DELETE FROM paperless_comments');
  yield client.queryPromise('DELETE FROM assignment_grades');
  yield client.queryPromise('DELETE FROM submissions');
  yield client.queryPromise('DELETE FROM assignments').then(function() {console.log("YAY")});

  yield client.queryPromise('DELETE FROM global_state');

  yield client.queryPromise('DELETE FROM helper_shifts');
  yield client.queryPromise('DELETE FROM helper_assignments');
  yield client.queryPromise('DELETE FROM helper_checkins');
  yield client.queryPromise('DELETE FROM help_requests').then(function() {console.log("YAY")});

  yield client.queryPromise('DELETE FROM student_relations');
  yield client.queryPromise('DELETE FROM staff_relations').then(function() {console.log("YAY")});
  yield client.queryPromise('DELETE FROM group_relations');

  yield client.queryPromise('DELETE FROM courses').then(function() {console.log("YAY")});
  yield client.queryPromise('DELETE FROM people').then(function() {console.log("NOPE")});
  yield client.queryPromise('DELETE FROM quarters').then(function() {console.log("NOPE")});

  client.end();
  console.log("All existing records cleared");
  done();
}).catch(function(ex) {
  console.log(ex); 
});
