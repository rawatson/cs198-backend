"use strict";
require('dotenv').config({path: __dirname + '/../.env'});
var co = require('co');

// Load all sync files
var quarterSync = require('./sync/quarters');
var peopleSync = require('./sync/people');
var coursesSync = require('./sync/courses');
var courseRelationsSync = require('./sync/course_relations');
var sectionsSync = require('./sync/sections');
var sectionAssignmentsSync = require('./sync/section_assignments');

// DB connection information
var mysql = require('mysql-co');
var pg = require('co-pg')(require('pg'));
var pgcs = process.env.DATABASE_URL;
var mysqlcs = {
  "host": process.env.LEGACY_DB_HOST,
  "user": process.env.LEGACY_DB_USER,
  "password": process.env.LEGACY_DB_PASSWORD,
  "database": process.env.LEGACY_DB_DATABASE
};

// Migrating legacy data
function migrate() {
  co(function*() {
    var mysqlc = mysql.createConnection(mysqlcs);
    var pgc = new pg.Client(pgcs);
    yield pgc.connectPromise();

    yield quarterSync.sync(pgc, mysqlc);

    yield [
      peopleSync.sync(pgc, mysqlc),
      coursesSync.sync(pgc, mysqlc)
    ];
    yield [
      courseRelationsSync.sync(pgc, mysqlc)
    ];
    yield sectionsSync.sync(pgc, mysqlc);
    yield sectionAssignmentsSync.sync(pgc, mysqlc);

    var has_global_state = false;
    var res = yield pgc.queryPromise(`SELECT COUNT(*) AS count FROM global_state`);
    if (res.rows[0].count == 0) {
      yield pgc.queryPromise(`
        INSERT INTO global_state
        SELECT false, MAX(id)
        FROM quarters
      `);
    }

    pgc.end();
    mysqlc.end();
  })
  .then(function(result) {
    console.log("ALL MIGRATIONS SUCCESSFUL");
  })
  .catch(function(err) {
    console.log("MIGRATION FAILED");
    console.log("MESSAGE: " + err);
    console.log(err.stack);
    console.log(err);
  })
}
migrate();

