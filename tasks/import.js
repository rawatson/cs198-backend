"use strict";
require('dotenv').config({path: __dirname + '/../.env'});
var co = require('co');


// DB connection information
var mysql = require('mysql-co');
var pg = require('co-pg')(require('pg'));
var pgcs = process.env.DATABASE_URL;
var mysqlcs = {
  "host": process.env.LEGACY_DB_HOST,
  "user": process.env.LEGACY_DB_USER,
  "password": process.env.LEGACY_DB_PASSWORD,
  "database": process.env.LEGACY_DB_DATABASE
};

// Migrating legacy data
function migrate() {
  co(function*() {
    var mysqlc = mysql.createConnection(mysqlcs);
    var pgc = new pg.Client(pgcs);
    var utils = require('./import/_utils');
    require('./import/course_relations');
    yield pgc.connectPromise();

    yield utils.cleanDatabase(pgc);
    yield require('./import/quarters').migrate(pgc, mysqlc),

    yield [
      require('./import/people').migrate(pgc, mysqlc),
      require('./import/courses').migrate(pgc, mysqlc)
    ];
    yield [
      require('./import/course_relations').migrate(pgc, mysqlc)
    ];

    yield [
      require('./import/seed_data').migrate(pgc, mysqlc)
    ];

    pgc.end();
    mysqlc.end();
  })
  .then(function(result) {
    console.log("ALL MIGRATIONS SUCCESSFUL");
  })
  .catch(function(err) {
    console.log("MIGRATION FAILED");
    console.log("MESSAGE: " + err);
    console.log(err.stack);
    console.log(err);
  })
}
migrate();

