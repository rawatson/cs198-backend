exports.up = function(pgm) {
  pgm.sql(`
    CREATE TYPE staff_position
    AS ENUM ('Course Helper',
             'Grader',
             'Section Leader',
             'Head TA',
             'Lecturer',
             'Coordinator');
  `);
  pgm.sql(`
  CREATE TABLE staff_relations (
    id serial PRIMARY KEY,
    person_id integer references people(id) NOT NULL,
    course_id integer references courses(id) NOT NULL,
    position staff_position NOT NULL,
    unique(person_id, course_id, position)
  );`);

};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE staff_relations`);
  pgm.sql(`DROP TYPE staff_position`);
};
