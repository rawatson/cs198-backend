exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE group_relations (
    id serial PRIMARY KEY,
    person_id integer references people(id) NOT NULL,
    course_id integer references courses(id) NOT NULL,
    group_name character varying NOT NULL,
    unique(group_name, course_id, person_id)
  );`);
};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE group_relations`);
};
