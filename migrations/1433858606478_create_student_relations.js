exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE student_relations (
    id serial PRIMARY KEY,
    person_id integer references people(id) NOT NULL,
    course_id integer references courses(id) NOT NULL,
    listed_in_axess boolean NOT NULL,
    unique(person_id, course_id)
  );`);
};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE student_relations`);
};
