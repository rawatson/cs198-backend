exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE helper_checkins (
    id serial PRIMARY KEY,
    person_id integer references people(id) NOT NULL,
    check_in_time timestamptz DEFAULT NOW() NOT NULL,
    check_out_time timestamptz
  );`);
};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE helper_checkins`);
};
