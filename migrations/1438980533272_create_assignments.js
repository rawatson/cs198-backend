exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE assignments (
    id serial PRIMARY KEY,
    course_id integer REFERENCES courses(id) NOT NULL,
    due_date timestamptz NOT NULL,
    late_dates JSON NOT NULL DEFAULT '[]'::json,
    assignment_name text NOT NULL,
    assignment_number real NOT NULL,
    required_files JSON NOT NULL DEFAULT '[]'::json,
    hidden_file_extensions JSON NOT NULL DEFAULT '[]'::json
  );`);
};

exports.down = function(pgm) {
  pgm.sql(`DROP table assignments`);
};
