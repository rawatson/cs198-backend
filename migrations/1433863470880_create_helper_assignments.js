exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE helper_assignments (
    id serial PRIMARY KEY,
    helper_checkin_id integer references helper_checkins(id) NOT NULL,
    help_request_id integer references help_requests(id) NOT NULL,
    claim_time timestamptz DEFAULT NOW() NOT NULL,
    close_time timestamptz,
    reassignment_id integer references helper_assignments(id),
    UNIQUE(helper_checkin_id, close_time)
  );`);
};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE helper_assignments`);
};
