exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE help_requests (
    id serial PRIMARY KEY,
    student_relation_id integer references student_relations(id) NOT NULL,
    problem_description character varying NOT NULL,
    location character varying NOT NULL,
    request_time timestamptz DEFAULT NOW() NOT NULL
  );`);

};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE help_requests`);
};
