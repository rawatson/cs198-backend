exports.up = function(pgm) {
  pgm.sql(`
  CREATE TYPE citizen_status
  AS ENUM ('US Citizen',
           'Permanent Resident',
           'International',
           'Unknown')`);

  pgm.sql(`
  CREATE TYPE gender
  AS ENUM ('Male',
           'Female',
           'Other',
           'Unknown')`);

  pgm.sql(`
  CREATE TYPE staff_status
  AS ENUM ('Non-Staff',
           'Active',
           'Inactive / Alumni',
           'Courtesy',
           'Bad Standing')`);

  pgm.sql(`
  CREATE TABLE people (
    id serial PRIMARY KEY,
    suid integer UNIQUE NOT NULL,
    sunetid character varying UNIQUE NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    nick_name character varying,
    email character varying NOT NULL,
    phone_number character varying,
    gender gender NOT NULL DEFAULT 'Unknown',
    citizen_status citizen_status NOT NULL DEFAULT 'Unknown',
    staff_status staff_status NOT NULL DEFAULT 'Non-Staff',
    hire_quarter_id integer references quarters(id)
  );`);
};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE people`);
  pgm.sql(`DROP TYPE staff_status`);
  pgm.sql(`DROP TYPE gender`);
  pgm.sql(`DROP TYPE citizen_status`);
};
