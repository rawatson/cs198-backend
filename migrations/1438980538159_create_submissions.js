exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE submissions (
    id serial PRIMARY KEY,
    student_relation_id integer REFERENCES student_relations(id) NOT NULL,
    assignment_id integer REFERENCES assignments(id) NOT NULL,
    submission_time timestamptz NOT NULL DEFAULT NOW(),
    submission_path text NOT NULL
  );`);
};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE submissions`);
};
