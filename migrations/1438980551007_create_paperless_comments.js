exports.up = function(pgm) {
  pgm.sql(`
  CREATE TYPE comment_types
  AS ENUM ('Functionality',
           'Style',
           'Other')`);
  pgm.sql(`
  CREATE TABLE paperless_comments (
   id integer PRIMARY KEY,
   start_line integer NOT NULL,
   end_line integer NOT NULL,
   comment_type comment_types,
   comment text);`); 
};

exports.down = function(pgm) {
  pgm.sql('DROP TABLE paperless_comments');
  pgm.sql('DROP TYPE comment_types');
};
