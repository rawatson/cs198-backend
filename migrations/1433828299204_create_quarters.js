exports.up = function(pgm) {
  pgm.sql(`
  CREATE TYPE quarter_names
  AS ENUM ('Winter',
           'Spring',
           'Summer',
           'Autumn')`);

  pgm.sql(`
  CREATE TABLE quarters (
    id integer PRIMARY KEY,
    year character varying NOT NULL,
    quarter_name quarter_names NOT NULL,
    unique(year, quarter_name)
  );`);
};

exports.down = function(pgm) {
  pgm.sql('DROP TABLE quarters');
  pgm.sql('DROP TYPE quarter_names');
};
