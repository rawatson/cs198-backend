exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE helper_shifts(
    id serial PRIMARY KEY,
    start_time timestamptz NOT NULL,
    duration_minutes integer NOT NULL,
    regular_shift boolean NOT NULL DEFAULT true,
    person_id integer references people(id) NOT NULL
  );`);

};

exports.down = function(pgm) {
  pgm.sql(`
    DROP TABLE helper_shifts;
  `);
};
