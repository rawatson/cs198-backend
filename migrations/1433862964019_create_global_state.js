exports.up = function(pgm) {
   pgm.sql(`
  CREATE TABLE global_state (
    lair_signups_enabled boolean NOT NULL,
    current_quarter_id int NOT NULL references quarters(id)
  );`);
};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE global_state`);
};
