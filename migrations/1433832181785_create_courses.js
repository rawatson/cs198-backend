exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE courses (
    id serial PRIMARY KEY,
    department_code character varying NOT NULL,
    course_number character varying NOT NULL,
    quarter_id integer references quarters(id) NOT NULL
  );`);
};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE courses`);
};
