exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE section_assignments (
    id serial PRIMARY KEY,
    section_id integer REFERENCES sections(id) NOT NULL,
    student_id integer references people(id) NOT NULL
  );`);

};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE section_assignments`);
};
