exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE assignment_grades (
    id serial PRIMARY KEY,
    assignment_id integer REFERENCES assignments(id) NOT NULL,
    student_relation_id integer REFERENCES student_relations(id) NOT NULL,
    functionality_grade text,
    style_grade text,
    late_days_used integer
  );`);
};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE assignment_grades`);
};
