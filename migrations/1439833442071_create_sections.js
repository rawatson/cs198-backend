exports.up = function(pgm) {
  pgm.sql(`
  CREATE TABLE sections (
    id serial PRIMARY KEY,
    course_id integer REFERENCES courses(id) NOT NULL,
    section_leader_id integer references people(id)
  );`);
  // section_leader_id may be NULL, since sections might not yet be assigned an SL
};

exports.down = function(pgm) {
  pgm.sql(`DROP TABLE sections`);
};
